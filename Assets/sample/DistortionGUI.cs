using UnityEngine;
using System.Collections;

public class DistortionGUI : MonoBehaviour {
	
	public CustomDistortionFilter filter_target;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnGUI () {
		GUILayout.BeginVertical( GUILayout.Width(Screen.width) );
		GUILayout.Label("PRE GAIN: " + filter_target.pre_gain.ToString() );
		filter_target.pre_gain = GUILayout.HorizontalSlider( filter_target.pre_gain, 0.0f, 100);
		GUILayout.Label("THRESHOLD: " + filter_target.threshold.ToString() );
		filter_target.threshold = GUILayout.HorizontalSlider( filter_target.threshold, 0.0f, 1);
		GUILayout.Label("ASYMMETRIC FACTOR: " + filter_target.asymmetric_factor.ToString() );
		filter_target.asymmetric_factor = GUILayout.HorizontalSlider( filter_target.asymmetric_factor, 0.0f, 1);
		GUILayout.Label("POST GAIN: " + filter_target.post_gain.ToString() );
		filter_target.post_gain = GUILayout.HorizontalSlider( filter_target.post_gain, 0.0f, 5 );
		GUILayout.EndVertical();
	}
}
